import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, children, Button } from 'react-native';
import { StatusBar } from 'expo-status-bar';

export default ({ navigation }) => {
    return (
        <View style={styles.container}>

        <StatusBar style="auto" />

        <Image
            style={styles.image}
            source={{ uri: 'https://i.pinimg.com/originals/84/b6/d3/84b6d3c51f8aa04b7261c85edd5c4ec0.jpg' }} />


        <Text style={styles.text}>Bienvenidos!</Text>
        
        <Text style={styles.texto}>POST es una aplicación diseñada que contiene contenido, artículo, opinión, noticia u otro género, que un autor publica.</Text>

        <TouchableOpacity style={styles.textobutton}
            style={styles.button}
            onPress={() => navigation.navigate('Users')} 
        >
            <Text> Entrar ->  {children}  
            
            </Text>
            
        </TouchableOpacity>

    </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: "column",

    },
    tinyLogo: {
        width: 500,
        height: 550,
    },
    image: {
        flex: 2,
        justifyContent: "center",
        width: '100%',
        height: '100%',
        position: 'absolute',
    },
    text: {
        color: "white",
        fontSize: 42,
        fontWeight: "bold",
        backgroundColor: "#000000a0",
        position: 'relative',
        padding: 5,
        margin: 35
    },
    texto: {
        color: "white",
        fontSize: 20,
        fontWeight: "bold",
        backgroundColor: "#fc7b5f",
        position: 'relative',
        padding: 5,
        margin: 80
    },
    
    button: {
        alignItems: "center",
        backgroundColor: "#FF5733",
        paddingVertical: 10,
        paddingHorizontal: 40,
        borderRadius: 8,  
        fontWeight: "bold",
        
    },
});