import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { StatusBar } from 'expo-status-bar';

export default ({ navigation}) => {
  const body = navigation.getParam('body')
  const title = navigation.getParam('title')
  const name = navigation.getParam('name')
    return (
      <View style={styles.container}>
        <Text style={styles.prin} >{name}</Text>
        <Text style={styles.secun}>{title}</Text>
        <Text style={styles.cuerp}>{body}</Text>
        <StatusBar style="auto" />
      </View>
    );
  }

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#eed684',
      alignItems: 'center',
      justifyContent: 'center',
      padding: 5
    },
    prin:{
      color:'#C70039',
      fontSize:40,
      fontWeight: "bold",
    },
    secun:{
      color:'#e67e22',
      fontSize:30,
    },
    cuerp:{
      fontSize:20,
    },
    
  });
  
  