import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import UsersScreen from './src/screens/Users';
import PostsScreen from './src/screens/PostsScreen';
import DetailScreen from './src/screens/DetailScreen';
import IntroScreen from './src/screens/IntroScreen';

const AppNavigator = createStackNavigator({
  Intro:{
    screen: IntroScreen,
  },
  Users: {
    screen: UsersScreen,
  },
  Posts: {
    screen: PostsScreen,
  },
  Detail: {
    screen: DetailScreen,
  }
}, {
  initialRouteName: 'Intro',
})


export default createAppContainer(AppNavigator)

